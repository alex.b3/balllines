﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManageBalls : MonoBehaviour
{

    // Корутина, которая ожидает окончания анимации
    // И затем удаляет мачики в соответствии со списком
    public IEnumerator DeleteBallsAfterSeconds(float waitTime, List<Vector2Int> ballsToDestroy)
    {
        // ждем
        yield return new WaitForSeconds(waitTime);
        // Теперь нужно удалить шарики
        foreach (Vector2Int destroyBall in ballsToDestroy)
        {
            // Получаем объект (префаб на сцене) с шариком
            GameObject destroy = grid[destroyBall.x, destroyBall.y];
            // Если объект по координатам есть, то уничтожаем
            if (destroy != null)
            {
                // Очищаем ячейку поля
                grid[destroyBall.x, destroyBall.y] = null;
                // Уничтожаем объект
                Destroy(destroy);
            }
        }
        // Очищаем массив, ведь мы все шарики удалили
        ballsToDestroy.Clear();
    }

    // Перечисляем направления
    enum Direction : byte { Vertical, Horizontal, DiagonalOne, DiagonalTwo };
    // Проверяем все варианты сборки шариков в линию
    // И возвращаем истину, если шарики собрались и список шариков, которые нужно убрать с поля
    public bool BallsCollected(int toPosX, int toPosY, out List<Vector2Int> returnBallsRow)
    {

        bool returnResult = false;
        // Шарики по этим коодинатам нужно удалить, игрок их собрал
        // мы вернем список этих шариков
        returnBallsRow = new List<Vector2Int>();
        for (int i = 0; i < 4; i++)
        {
            // Проходимся по 4 направлениям по очереди
            List<Vector2Int> localBallsRow = GetRow(toPosX, toPosY, (Direction)i);
            // Объединяем массивы в один
            if (localBallsRow.Count > 4)
            {
                /*if (i == 0) returnBallsRow = localBallsRow;
                else*/
                returnBallsRow.AddRange(localBallsRow);
                returnResult = true;
            }
        }
        return returnResult;
    }

    // Проверяем сборку шариков в линию
    List<Vector2Int> GetRow(int toPosX, int toPosY, Direction direction)
    {
        // Будем использовать вектора для всех действий, так проще.
        Vector2Int positionVector = new Vector2Int(toPosX, toPosY), offsetVector = new Vector2Int(0, 0);
        // Создаем вектор смещения на основе направлений
        switch (direction)
        {
            case Direction.Horizontal:
                // x = 1 (вправо)
                offsetVector = new Vector2Int(1, 0);
                break;
            case Direction.Vertical:
                // у = 1 (вверх)
                offsetVector = new Vector2Int(0, 1);
                break;
            case Direction.DiagonalOne:
                // x = 1 (вправо), у = 1 (вверх)
                offsetVector = new Vector2Int(1, 1);
                break;
            case Direction.DiagonalTwo:
                // x = 1 (вправо), у = -1 (вниз)
                offsetVector = new Vector2Int(1, -1);
                break;
        }

        // бесконечный цикл, выходим по результату
        // Ищем крайний шарик того же цвета
        while (true)
        {
            // Виртуально идем назад, к первому шарику
            positionVector -= offsetVector;
            if (outOffGrid(positionVector) || grid[positionVector.x, positionVector.y] == null || grid[positionVector.x, positionVector.y].name != grid[toPosX, toPosY].name)
            {
                // предыдущая позиция была последняя верная
                // возвращаем
                positionVector += offsetVector;
                break;
            }
        }

        List<Vector2Int> returnBallsRow = new List<Vector2Int>();

        while (true)
        {
            // Первый шарик точно в нужном цвете
            returnBallsRow.Add(new Vector2Int(positionVector.x, positionVector.y));
            // Теперь идем вперед
            positionVector += offsetVector;
            if (outOffGrid(positionVector) || grid[positionVector.x, positionVector.y] == null || grid[positionVector.x, positionVector.y].name != grid[toPosX, toPosY].name)
            {
                // предыдущая позиция была последняя верная
                // выходим, массив готов
                break;
            }
        }

        return returnBallsRow;
    }

    // Функция проверки выхода за границы поля
    private bool outOffGrid(Vector2Int positionVector)
    {
        // Проверяем, находится ли вектор внутри сетки или нет
        // Истина тогда, когда вектор за пределами (по названию функции outOffGrid)
        if (positionVector.x < 0 || positionVector.x >= width || positionVector.y < 0 || positionVector.y >= height) return true;
        return false;
    }
    

    // Сохраняем в эту переменную
    // Что что-то происходит и игрок ждет
    bool inProgress = false;

    // Вычисленное время,
    // За которое шарик должен дойти до клетки
    float computedSpeed = 0;

    // Текущий маршрут шарика
    List<Vector3> ballPath;

    // Временный список всех нарисованных цифр
    List<GameObject> indicators = new List<GameObject>();

    // Рисуем цифры
    // возвращаем маршрут
    private List<Vector3> GetRoute(int toPosX, int toPosY)
    {
        // доска для построения маршрута
        byte[,] gridPath = new byte[width, height];

        // Инициализируем массив расчета пути
        for (int i = 0; i < width; i++)
            for (int j = 0; j < height; j++)
                // Если шарик есть в клетке записываем 255
                if (grid[i, j] != null) gridPath[i, j] = 255;
                // Если нет - 0
                else gridPath[i, j] = 0;

        // Это у нас цепочка из всех значений
        List<Vector2Int> itterationIndicators = new List<Vector2Int>();

        // Идем от положения шарика
        byte indicateNumber = 1;
        // Добавляем в цепочку
        itterationIndicators.Add(new Vector2Int((int)selectedBall.transform.position.x, (int)selectedBall.transform.position.z));
        // Добавляем на поле
        gridPath[itterationIndicators[0].x, itterationIndicators[0].y] = indicateNumber;

        // Каждый шаг генерирует список (цепочку)
        // Если длина списка > 0
        // То нужен еще ожин проход
        while (itterationIndicators.Count > 0)
        {
            // Временный список для новых индикаторов
            List<Vector2Int> nextIndicators = new List<Vector2Int>();
            // Значение индикации увеличивыаем на каждом шаге, что логично
            indicateNumber++;
            // Теперь проходимся по текушим индикаторам по очереди
            foreach (Vector2Int indicator in itterationIndicators)
            {
                // Ход влево
                if (indicator.x - 1 >= 0 && gridPath[indicator.x - 1, indicator.y] == 0)
                {
                    // Добавляем в новый список (для следующей итерации)
                    nextIndicators.Add(new Vector2Int(indicator.x - 1, indicator.y));
                    // Сохраняем в таблицу
                    gridPath[indicator.x - 1, indicator.y] = indicateNumber;

                }
                // Ход вправо
                if (indicator.x + 1 < width && gridPath[indicator.x + 1, indicator.y] == 0)
                {
                    // Добавляем в новый список (для следующей итерации)
                    nextIndicators.Add(new Vector2Int(indicator.x + 1, indicator.y));
                    // Сохраняем в таблицу
                    gridPath[indicator.x + 1, indicator.y] = indicateNumber;
                }
                // Ход вниз
                if (indicator.y - 1 >= 0 && gridPath[indicator.x, indicator.y - 1] == 0)
                {
                    // Добавляем в новый список (для следующей итерации)
                    nextIndicators.Add(new Vector2Int(indicator.x, indicator.y - 1));
                    // Сохраняем в таблицу
                    gridPath[indicator.x, indicator.y - 1] = indicateNumber;
                }
                // Ход вверх
                if (indicator.y + 1 < height && gridPath[indicator.x, indicator.y + 1] == 0)
                {
                    // Добавляем в новый список (для следующей итерации)
                    nextIndicators.Add(new Vector2Int(indicator.x, indicator.y + 1));
                    // Сохраняем в таблицу
                    gridPath[indicator.x, indicator.y + 1] = indicateNumber;
                }
            }
            // Индикаторы накоплены для следующей итерации
            // Переписываем c основной список
            itterationIndicators = nextIndicators;
        }

        // Теперь осталось вернуть маршрут!
        // Инициализируем список маршрутных точек, который мы будем возвращать
        List<Vector3> returnPath = new List<Vector3>();
        // Считываем значение из положения курсора
        // Не заходим в цикл, если значение в выбранной точке = 0
        for (int value = gridPath[toPosX, toPosY]; gridPath[toPosX, toPosY] != 0;)
        {
            // Сохраняем координаты текущего положения
            returnPath.Add(new Vector3(toPosX, 0, toPosY));
            // Если значение в gridPath равно 2 выходим!
            // Нам в массиве не нужна текущая позиция шарика
            if (gridPath[toPosX, toPosY] == 2) break;
            value--; // Уменьшаем значение, ведь мы ищем следующее
                     // Проверка влево, что там следующее значение
            if (toPosX - 1 >= 0 && gridPath[toPosX - 1, toPosY] == value) toPosX--; // Переходим на новое положение
                                                                                    // Иначе вправо
            else if (toPosX + 1 < width && gridPath[toPosX + 1, toPosY] == value) toPosX++;
            // Иначе вниз
            else if (toPosY - 1 >= 0 && gridPath[toPosX, toPosY - 1] == value) toPosY--;
            // 100 вверх, можно даже не проверять!
            else toPosY++;
        }
        // переворачиваем список
        returnPath.Reverse();
        return returnPath; //возвращаем
    }


    // Генерация из префаба нового значения по координатам
    GameObject GenegateIndicator(byte indicate, int toPosX, int toPosY, bool greenColor)
    {
        // Таким способом мы загружаем префаб из ресурсов (специальная папка Resources)
        UnityEngine.Object indicatorPrefab = Resources.Load("Indicator");
        // Полученный объект клонируем на сцену
        GameObject indicator = (GameObject)Instantiate(indicatorPrefab);
        // Меняем его положение на нужную ячейку
        indicator.transform.position = new Vector3(toPosX, greenColor ? 0.001f : 0, toPosY);
        // Меняем порядковый номер
        indicator.transform.GetChild(0).GetComponent<Text>().text = indicate.ToString();
        // Рисуем зеленые индикаторы
        if (greenColor) indicator.transform.GetChild(0).GetComponent<Text>().color = new Color(0, 1, 0);
        // Возврящаем на него ссылку
        return indicator;
    }


    // добавляем камеру
    public Camera camera;

    // размер доски
    public static int width = 9;
    public static int height = 9;

    // доска с шариками
    public static GameObject[,] grid = new GameObject[width, height];

    // выбранный шарик(сфера)
    // стоит обратить внимание, что это именно сфера
    // а не префаб (т.е. корень)
    // Когда нам потребуется координаты на поле, 
    // нужно найти родителя сферы (корень)
    // также, если не писать видимость
    // то по умолчанию ДЛЯ КЛАССА!!!! 
    // она будет private
    GameObject selectedBall;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (inProgress)
        {
            // Новое положение вычисляется специальной функцией,
            // которая берет текущее положение (реальное) и будущее положение 
            // (не следующее, она его вычисляет, а то, в которое шарик должен когда-то дойти)
            // И рассчитывает координату где-то посередине, но строго в соответствии с пройденным временем
            // Общую скорость перемещения мы задаем коэффициентом computedSpeed
            // Time.deltaTime как раз достаточно точное время между кадрами
            selectedBall.transform.parent.position = Vector3.MoveTowards(selectedBall.transform.parent.position, ballPath[0], computedSpeed * Time.deltaTime);

            // Т.к. время кадра немного меняется, и Time.deltaTime плавает
            // То нет никакой гарантии, что положение, рассчитанное функцией MoveTowards
            // Математически совпадет с заданным (текущим первым в списке с координатами пути)
            // Поэтому проверку делаем через вычисление расстояния между фактическим положением
            // И желаемым, и если оно меньше 0.001, то считаем, что шарик прискакал в следующую клетку
            if (Vector3.Distance(selectedBall.transform.parent.position, ballPath[0]) < 0.001f)
            {
                // Нужно математически поставить мяч в целые координаты
                selectedBall.transform.parent.position = ballPath[0];
                // Координата из списка, находящаяся в самом начале достигнута
                // ballPath.Count - количество координат при удалении уменьшится, а во главе списка
                // будет следующая координата (или координаты кончатся)
                ballPath.RemoveAt(0);

                // Мяч прискакал на предпоследнюю клетку
                if (ballPath.Count == 1)
                {
                    // Начинаем останавливать анимацию прыжка
                    // Используем плавный переход за секунду
                    selectedBall.GetComponent<Animator>().CrossFadeInFixedTime("Idle", 1.0f);
                }

                // Мяч прискакал, т.к. больше координат в списке нет
                // нужно это обработать
                if (ballPath.Count == 0)
                {
                    // Записываем корень мяча в новую позицию
                    grid[(int)selectedBall.transform.parent.position.x, (int)selectedBall.transform.parent.position.z] = selectedBall.transform.parent.gameObject;

                    // Проверяем с помощью функции собрал ли игрок 5+ шариков в ряд
                    // Пока функции нет, поэтому вместо функции
                    // в проверку вписан false
                    if (BallsCollected((int)selectedBall.transform.parent.position.x, (int)selectedBall.transform.parent.position.z, out List<Vector2Int> victoryBallsToDestroy))
                    {
                        // Тут будем запускать анимацию исчезновения шариков и их удаление с поля
                        // Сначала нужно запустить анимацию, проходимся по списку координат шариков и запускаем анимацию
                        foreach (Vector2Int animateDestroyBall in victoryBallsToDestroy)
                            grid[animateDestroyBall.x, animateDestroyBall.y].GetComponentInChildren<Animator>().Play("Destroy");
                        // А теперь удалить объекты
                        // Делаем это простым вызовом корутины
                        StartCoroutine(DeleteBallsAfterSeconds(1f, victoryBallsToDestroy));
                        // Снимаем ожидание хода
                        inProgress = false;
                    }

                    else // Выигрыша нет
                    {
                        // Для случая, когда ход был в соседнюю клетку
                        // ballPath.Count на первом шаге уже 1, и когда мы удаляем 
                        // координату из списка, он становится равным 0
                        // Нужно остановить анимацию
                        selectedBall.GetComponent<Animator>().Play("Idle");

                        // Убираем выделение мяча
                        selectedBall = null;

                        // Генерируем три меча и проверяем на конец игры
                        if (!GetComponent<SpawnBalls>().GenerateBalls())
                        {
                            // Тут конец игры!!!
                            // Который мы не реализовали
                        }

                        inProgress = false; // Игра ждем ввода игрока
                    }
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            RaycastHit hit; // Точка попадания мышкой
                            // луч, выпущенный из камеры по координатам мышки
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);
            // Если луч пересекся с любым объектом, 
            // то можно что-то проверять дальше!
            if (Physics.Raycast(ray, out hit))
            {
                // У нас глобально два типа объектов:
                // 1. доска, у всех объектов Y координата = -1
                // 2. шарик
                // получаем простое сравнение!
                if (hit.transform.position.y == -1)
                {
                    // Точно доска!!!
                    // Шарик выбран и можно начать его перемещение
                    // На выбранное пустое!!! поле
                    if (selectedBall != null && grid[(int)hit.transform.position.x, (int)hit.transform.position.z] == null)
                    {
                        // Пока просто визуализируем маршрут
                        //  DebugVisualizeRoute((int)hit.transform.position.x, (int)hit.transform.position.z);
                        // Получаем маршрут шарика
                        ballPath = GetRoute((int)hit.transform.position.x, (int)hit.transform.position.z);
                        // Если в маршруте есть шаги, переходим к перемещению шарика
                        if (ballPath.Count > 0)
                        {
                            inProgress = true; // Игра занята следующим ходом
                                               // Удаляем мяч (корень выделенного мяча) из старой позиции
                                               // Помним, что выделенный мяч (сфера) это не корень префаба
                                               // И координаты нужно взять у родительского объекта
                            grid[(int)selectedBall.transform.parent.position.x, (int)selectedBall.transform.parent.position.z] = null;
                            // Мы хотим получить время от 1 до 3 секунд
                            // Причем не линейно, до 5 шагов примерно 1
                            // До 10 шагов примерно 2
                            // И все что больше примерно 3
                            if (ballPath.Count < 6) computedSpeed = 0.7f + ((float)ballPath.Count) * 0.5f;
                            else if (ballPath.Count < 11) computedSpeed = 1.5f + ((float)ballPath.Count) * 0.5f;
                            else computedSpeed = 2f + ((float)ballPath.Count) * 0.5f;
                        }

                    }

                }

                else
                {
                    // Точно шарик!!!
                    // Останавливаем анимацию для ранее выделенного шара
                    // Только если выбран не тот же шар
                    if (selectedBall != null && selectedBall != hit.transform.gameObject)
                    {
                        selectedBall.GetComponent<Animator>().Play("Idle");
                    }
                    // Добираемся до указателя на объект
                    selectedBall = hit.transform.gameObject;

                    // Запускаем анимацию прыжка
                    selectedBall.GetComponent<Animator>().Play("Bouncing");


                }
            }
        }

    }

}
