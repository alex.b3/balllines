﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBalls : MonoBehaviour
{
    public GameObject[] allVersionsBalls;

    // Функция генерации шариков на поле
    public bool GenerateBalls()
    {
        // Нунжно уместить 3 шарика
        for (int i = 0; i < 3; i++)
        {
            if (!GenerateOneBall()) return false;
        }
        return true; // Всем шарикам хватило места

    }

    // Функция сохранения одного шарика в ячейку
    bool GenerateOneBall()
    {
        // Посчитаем количество пустых ячеек
        int freeCells = 0;
        for (int i = 0; i < ManageBalls.width; i++)
            for (int j = 0; j < ManageBalls.height; j++)
                if (ManageBalls.grid[i, j] == null) freeCells++;

        // Если пустых ячеек нет, значит конец!
        if (freeCells == 0) return false;

        // Выбираем случайную ячейку из диапазона
        // от нуля до максимально доступной
        int randomCell = Random.Range(0, freeCells);
        // Преобразуем из порядкового номера в индекс (координаты)
        int randomCellWidthPosition = 0;
        int randomCellHeightPosition = 0;
        freeCells = 0;
        for (randomCellWidthPosition = 0; randomCellWidthPosition < ManageBalls.width; randomCellWidthPosition++)
            for (randomCellHeightPosition = 0; randomCellHeightPosition < ManageBalls.height; randomCellHeightPosition++)
                if (ManageBalls.grid[randomCellWidthPosition, randomCellHeightPosition] == null)
                    if (freeCells++ == randomCell)
                        goto LoopEnd; // Как только дошли до пустой ячейки по порядковому номеру, выходим
                    LoopEnd:
        // По адресу ячейки создаем шарик Instantiate не знает о типе, поэтому приводим к (GameObject)
        ManageBalls.grid[randomCellWidthPosition, randomCellHeightPosition] = (GameObject)Instantiate(
                allVersionsBalls[Random.Range(0, allVersionsBalls.Length)], // Вариант шарика
                new Vector3(randomCellWidthPosition, 0, randomCellHeightPosition),
                Quaternion.identity); // поворот идентичный повороту доски
        return true;
    }


    // Start is called before the first frame update
    void Start()
    {
        
        GenerateBalls();
        

    }

    // Update is called once per frame
    void Update()
    {
        
    }

}
